package com.otw.contactms.exception;


public class ContactException extends Throwable {
    public ContactException(String message) {
        super(message);
    }
}
