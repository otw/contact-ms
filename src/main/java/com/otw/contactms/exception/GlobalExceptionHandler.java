package com.otw.contactms.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ContactException.class})
    protected ResponseEntity<?> handleNodeNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, "The requested item was not found", new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
