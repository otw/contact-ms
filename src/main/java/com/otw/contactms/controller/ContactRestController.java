package com.otw.contactms.controller;

import com.otw.contactms.dto.ConnectedContactDTO;
import com.otw.contactms.dto.ContactDTO;
import com.otw.contactms.dto.ContactNameDTO;
import com.otw.contactms.dto.UpdateContactDTO;
import com.otw.contactms.exception.ContactException;
import com.otw.contactms.model.Contact;
import com.otw.contactms.service.ContactService;
import org.hibernate.sql.Update;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/contact")
public class ContactRestController {
    private final ContactService contactService;
    private final ModelMapper modelMapper;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    public ContactRestController(ContactService contactService, ModelMapper modelMapper) {
        this.contactService = contactService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/user")
    ResponseEntity<ContactDTO> getUser(@RequestHeader("User-ID") Long id) throws ContactException {
        Contact appUser = contactService.findById(id);
        return new ResponseEntity<>(modelMapper.map(appUser, ContactDTO.class), HttpStatus.OK);
    }

    @GetMapping("/getContact/{id}")
    ResponseEntity<ConnectedContactDTO> getContact(@PathVariable Long id) throws ContactException {
        Contact appUser = contactService.findById(id);
        return new ResponseEntity<>(modelMapper.map(appUser, ConnectedContactDTO.class), HttpStatus.OK);
    }

    @GetMapping("/connectedUsers")
    ResponseEntity<List<ConnectedContactDTO>> getConnectedUsers(@RequestParam(name = "userids") List<String> userids) throws ContactException {
        List<Contact> contacts = new ArrayList<>();
        for (String userId : userids) {
            Contact contact = contactService.findById(Long.parseLong(userId));
            contacts.add(contact);
        }

        List<ConnectedContactDTO> contactDTOList = new ArrayList<>();

        for (Contact contact : contacts) {
            contactDTOList.add(modelMapper.map(contact, ConnectedContactDTO.class));
        }

        return new ResponseEntity(contactDTOList, HttpStatus.OK);
    }

    @GetMapping("/connectedNames")
    ResponseEntity<List<ContactNameDTO>> getConnectedNames(@RequestParam(name = "userids") List<Long> userids) throws ContactException {
        List<ContactNameDTO> contactNameDTOS = new ArrayList<>();
        for (Long id : userids) {
            Contact contact = contactService.findById(id);
            contactNameDTOS.add(new ContactNameDTO(id, contact.getFirstname()));
        }
        return new ResponseEntity<>(contactNameDTOS, HttpStatus.OK);
    }


    @PostMapping("/create")
    public ResponseEntity<ContactDTO> createContact(@RequestBody ContactDTO contactDTO) {
        Contact contact = modelMapper.map(contactDTO, Contact.class);
        Contact contactOut = contactService.save(contact);
        return new ResponseEntity<>(modelMapper.map(contactOut, ContactDTO.class), HttpStatus.CREATED);

    }

    @PutMapping("/update")
    public ResponseEntity updateContact(@RequestHeader("User-ID") Long userId, @RequestBody UpdateContactDTO contactDTO) throws ContactException {
        contactDTO.setPassword(passwordEncoder.encode(contactDTO.getPassword()));
        Contact contactOut = contactService.update(userId, modelMapper.map(contactDTO, Contact.class));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @PostMapping("/registration")
    ResponseEntity<ContactDTO> registerUser(@RequestBody ContactDTO appUserDto) throws URISyntaxException, ContactException {
        if (contactService.findByEmail(appUserDto.getEmail()) == null) {
            Contact contact = modelMapper.map(appUserDto, Contact.class);

            contact.setPassword(passwordEncoder.encode(appUserDto.getPassword()));
            contact.setRole("USER");
            contactService.save(contact);
            return new ResponseEntity<>(appUserDto, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


}
