package com.otw.contactms;

import com.otw.contactms.model.Contact;
import com.otw.contactms.repository.ContactRepository;
import com.otw.contactms.service.ContactService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ContactMsApplication {


    public static void main(String[] args) {
        SpringApplication.run(ContactMsApplication.class, args);

    }

}

