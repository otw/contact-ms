package com.otw.contactms;

import com.otw.contactms.model.Contact;
import com.otw.contactms.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ContactmssComponent implements CommandLineRunner {

    private ContactService contactService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public ContactmssComponent(ContactService contactService) {
        this.contactService = contactService;
    }

    @Override
    public void run(String... args) {
        //init
        byte[] testjen = "test".getBytes();
        contactService.save(new Contact("Sige", "VV", "testing@gmail.com", passwordEncoder.encode("12345"), "USER", "0413475214", "Passionate about my work, in love with my family and dedicated to spreading light." +
                "Instagram - @JanDeBakker"));
        contactService.save(new Contact("Jelle", "De Smit", "jelleds@gmail.com", passwordEncoder.encode("12345"), "USER", "0452147895", "Passionate about my work, in love with my family and dedicated to spreading light."));
        contactService.save(new Contact("Jonas", "Scheldeman", "jonas@gmail.com", passwordEncoder.encode("12345"), "USER", "0477452795", "Klj leider"));
        contactService.save(new Contact("Pieter-Jan", "De Cauwer", "pj@gmail.com", passwordEncoder.encode("12345"), "USER", "0477452795", "Tren setter"));
    }
}
