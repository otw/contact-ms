package com.otw.contactms.repository;

import com.otw.contactms.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    Contact findByEmail(String email);

}
