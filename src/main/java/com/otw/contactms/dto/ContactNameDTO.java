package com.otw.contactms.dto;

public class ContactNameDTO {

    private Long nodeId;
    private String firstName;

    public ContactNameDTO(){}

    public ContactNameDTO(Long nodeId, String firstName) {
        this.nodeId = nodeId;
        this.firstName = firstName;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
