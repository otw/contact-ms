package com.otw.contactms.service;

import com.otw.contactms.dto.ContactDTO;
import com.otw.contactms.exception.ContactException;
import com.otw.contactms.model.Contact;
import com.otw.contactms.repository.ContactRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContactService {
    private final ContactRepository contactRepository;

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public Contact save(Contact contact) {
        return contactRepository.save(contact);
    }

    public Contact findByEmail(String email) {
        return contactRepository.findByEmail(email);
    }

    public Contact findById(Long id) throws ContactException {
        Optional<Contact> contact = contactRepository.findById(id);
        if (contact.isPresent()) {
            return contact.get();
        }
        throw new ContactException("Contact not found!");
    }

    public Long findUserIdByEmail(String email) {
        return this.findByEmail(email).getId();
    }

    public Contact update(Long userId, Contact contact) throws ContactException {
        Contact contactToUpdate = this.findById(userId);
        contactToUpdate.setFirstname(contact.getFirstname());
        contactToUpdate.setLastname(contact.getLastname());
        contactToUpdate.setPhoneNumber(contact.getPhoneNumber());
        contactToUpdate.setDescription(contact.getDescription());
        contactToUpdate.setPassword(contact.getPassword());
        return this.save(contactToUpdate);

    }
}
