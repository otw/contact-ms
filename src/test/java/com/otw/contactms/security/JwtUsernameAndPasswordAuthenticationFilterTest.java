package com.otw.contactms.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otw.contactms.dto.ContactDTO;
import com.otw.contactms.model.Contact;
import com.otw.contactms.model.UserCredentials;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JwtUsernameAndPasswordAuthenticationFilterTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ObjectMapper objectMapper;
    @Test
    public void loginFail() throws Exception {
        UserCredentials credentials = new UserCredentials("firstlast@mail.com", "testpwd");
        String postJson = objectMapper.writeValueAsString(credentials);
        mockMvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJson))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void registerAndLoginShouldSucceed() throws Exception {

        Contact appUser = new Contact("voornaam", "achternaam", "firstlast@mail.com", "testpwd", "ROLE_USER","0488888888", "desc");
        ContactDTO userDto = modelMapper.map(appUser, ContactDTO.class);
        String postJson = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(post("/contact/registration")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJson))
                .andExpect(status().isCreated());

        UserCredentials credentials = new UserCredentials("firstlast@mail.com", "testpwd");
        String postLogin = objectMapper.writeValueAsString(credentials);

        mockMvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postLogin))
                .andExpect(status().isOk());

    }

}
